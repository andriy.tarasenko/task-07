DROP TABLE IF EXISTS users_teams;
DROP TABLE IF EXISTS teams;
DROP TABLE IF EXISTS users;


CREATE TABLE IF NOT EXISTS users (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	login VARCHAR(10) UNIQUE
);

CREATE TABLE IF NOT EXISTS teams (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name VARCHAR(10)
);

CREATE TABLE IF NOT EXISTS users_teams (
	user_id INTEGER REFERENCES users(id) on delete cascade,
	team_id INTEGER REFERENCES teams(id) on delete cascade,
	UNIQUE (user_id, team_id)
);

INSERT INTO users VALUES (0, 'ivanov');
INSERT INTO teams VALUES (0, 'teamA');

--SELECT * FROM users;
--SELECT * FROM teams;
--SELECT * FROM users_teams;

