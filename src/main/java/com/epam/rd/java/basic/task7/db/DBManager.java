package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;

import java.sql.*;


public class DBManager {

    //QUERIES
    static final String INSERT_USER = "INSERT INTO users (login) VALUES (?)";
    static final String GET_USER = "SELECT login, id FROM users WHERE login = ?";
    static final String DELETE_USERS = "DELETE FROM users WHERE login = ?";
    static final String SELECT_ALL_USERS = "SELECT * FROM users ORDER BY id";
    static final String INSERT_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?,?)";
    static final String GET_USER_TEAMS = "SELECT id, name FROM teams WHERE id IN " +
            "(SELECT team_id FROM users_teams WHERE user_id = ?)";
    static final String INSERT_TEAM = "INSERT INTO teams (name) VALUES (?)";
    static final String GET_TEAM = "SELECT name, id FROM teams WHERE name = ?";
    static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
    static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
    static final String SELECT_ALL_TEAMS = "SELECT * FROM teams ORDER BY id";


    private static DBManager instance;
    private Connection connection;

    public static synchronized DBManager getInstance() {

        if (instance == null) {
            try {
                instance = new DBManager();
            } catch (IOException e) {
                System.out.println(e);
            }
        }
        return instance;
    }

    private DBManager() throws IOException {
        Properties properties = loadProperties();
        String url = properties.getProperty("connection.url");

        try {
            connection = DriverManager.getConnection(url);

            System.out.println("Connection established");

//            Statement statement = connection.createStatement();
//
//            File query = new File(System.getProperty("user.dir") + "\\sql\\db-create.sql");
//            String sql = Files.readString(query.toPath(), Charset.defaultCharset());
//            System.out.println("Trying to erase database");
//            statement.executeUpdate(sql);
//
//            statement.close();
        } catch (SQLException e) {
            System.out.println("Constructor failure..\n" + e);
        }
    }

    public List<User> findAllUsers() throws DBException {
        System.out.println("findAllUsers call");
        List<User> resultList = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {


            ResultSet resultSet = statement.executeQuery(SELECT_ALL_USERS);

            while (resultSet.next()) {
                User user = User.createUser(resultSet.getString("login"));
                user.setId(resultSet.getInt("id"));
                resultList.add(user);
            }

        } catch (SQLException e) {
            throw new DBException("Find all users", e);
        }
        return resultList;
    }

    public boolean insertUser(User user) throws DBException {

        if (!user.getLogin().matches("\\w+") || user.getLogin().length() > 10)
            throw new IllegalArgumentException("wrong login format");

        try (PreparedStatement statement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, user.getLogin());

            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();

            if (resultSet.next()) user.setId(resultSet.getInt(1));

        } catch (SQLException e) {
            throw new DBException("Insert User fail ", e);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        System.out.println("deleteUsers call");
        try (PreparedStatement statement = connection.prepareStatement(DELETE_USERS)) {

            for (User user : users) {
                statement.setString(1, user.getLogin());
                statement.executeUpdate();
            }

        } catch (SQLException e) {
            throw new DBException("delete users fail", e);
        }

        return true;
    }

    public User getUser(String login) throws DBException {

        User user = null;
        if (!login.matches("\\w+") || login.length() > 10) throw new IllegalArgumentException("Wrong login format!");

        try (PreparedStatement statement = connection.prepareStatement(GET_USER)) {

            statement.setString(1, login);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                user = User.createUser(login);
                user.setId(resultSet.getInt("id"));
            }

        } catch (SQLException e) {
            throw new DBException("Get user fail", e);
        }

        return user;
    }

    public Team getTeam(String name) throws DBException {

        Team team = null;
        if (!name.matches("\\w+") || name.length() > 10)
            throw new IllegalArgumentException("wrong team name format!");

        try (PreparedStatement statement = connection.prepareStatement(GET_TEAM)) {

            statement.setString(1, name);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                team = Team.createTeam(name);
                team.setId(resultSet.getInt("id"));
            }
        } catch (SQLException e) {
            throw new DBException("Get team fail ", e);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {

        List<Team> teams = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(SELECT_ALL_TEAMS);

            while (resultSet.next()) {
                Team team = Team.createTeam(resultSet.getString("name"));
                team.setId(resultSet.getInt("id"));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException("find teams fail ", e);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {

        if (!team.getName().matches("\\w+") || team.getName().length() > 10)
            throw new IllegalArgumentException("wrong login format");

        try (PreparedStatement statement = connection.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {


            statement.setString(1, team.getName());

            statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();

            if (resultSet.next()) team.setId(resultSet.getInt(1));

        } catch (SQLException e) {
            throw new DBException("Insert team fail ", e);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        if (user == null || Arrays.stream(teams).anyMatch(Objects::isNull))
            throw new IllegalArgumentException("Bad input.");
        try {
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            connection.setAutoCommit(false);


            try (PreparedStatement statement = connection.prepareStatement(INSERT_TEAMS_FOR_USER)) {

                for (Team team : teams) {
                    statement.setInt(1, user.getId());
                    statement.setInt(2, team.getId());

                    statement.executeUpdate();
                }

                connection.commit();
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                try {
                    connection.rollback();
                    throw new SQLException(e);
                } catch (SQLException sql) {
                    throw new SQLException(sql);
                }

            }finally {
                connection.setAutoCommit(true);
            }
        }catch (SQLException sq){
            throw new DBException("Set Teams fail for:" + user.getLogin(),sq);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_USER_TEAMS)) {

            statement.setInt(1, user.getId());

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Team team = Team.createTeam(resultSet.getString("name"));
                team.setId(resultSet.getInt("id"));
                teams.add(team);
            }

        } catch (SQLException e) {
            throw new DBException("get user teams fail", e);
        }

        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (PreparedStatement statement = connection.prepareStatement(DELETE_TEAM)) {

            statement.setInt(1, team.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("delete team fail with", e);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_TEAM)) {

            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("update team fail", e);
        }

        return true;
    }

    private static Properties loadProperties() throws IOException {
        Properties properties = new Properties();

        try (InputStream inputStream = new FileInputStream("app.properties")) {
            properties.load(inputStream);
        }

        return properties;
    }
}
